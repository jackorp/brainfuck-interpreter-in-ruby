require_relative('scanner')

class Jump
  attr_accessor :target

  def initialize(target)
    @target = target
  end

  def to_s()
    self.inspect
  end
end

class ForwardJump < Jump; end
class BackwardJump < Jump; end

# Responsible for creating instructions
class Parser
  attr_reader :symbols
  def initialize(scanner_out)
    @input = scanner_out
    @symbols = parse
    figure_out_jumps!(@symbols)
    # require 'irb';binding.irb
    @symbols.flatten!
  end

  def parse
    # helps figure out what jump to pair with what jump
    jmp_stack = []
    # Index is going to serve as instruction pointer
    @input.map.with_index do |instruction, idx|
      if instruction == 'inc_pointer'
        :next_data
      elsif instruction == 'dec_pointer'
        :prev_data
      elsif instruction == 'inc_byte'
        :inc
      elsif instruction == 'dec_byte'
        :dec
      elsif instruction == 'out_byte'
        :p_char
      elsif instruction == 'get_byte'
        :getc
      elsif instruction == 'jmp_forward'
        ForwardJump.new(0)
      elsif instruction == 'jmp_back'
        BackwardJump.new(0)
      end
    end
  end

  def figure_out_jumps!(symbols)
    # FIFO
    stack = []

    symbols
      .each_with_index
      .find_all { |e| [ForwardJump, BackwardJump].any? { |a| e[0].is_a? a }  }
      .each do |symb|
        if symb[0].is_a? ForwardJump
          stack.append(symb)
        elsif symb[0].is_a? BackwardJump
          e = stack.pop
          # +1 to not jump on the jump
          e[0].target = (symb[1])
          symb[0].target = (e[1])
        end
      end
  end
end

class Interpreter
  def initialize(symbol_stream)
    @data = Array.new(1024) {0}
    @prev_max_data_pointer = 1023
    @data_pointer = 0
    @instructions = symbol_stream
    @instruction_pointer = 0
    @start = Time.now
    @iters = 0
  end

  def start
    # pp @instructions.each_with_index.to_a
    while @instructions.size > @instruction_pointer do
      dispatch(@instructions[@instruction_pointer])

      @instruction_pointer += 1
    end
  end

  def dispatch(instruction)
    # puts "inst: #{@instructions[@instruction_pointer]} inst_ptr: #{@instruction_pointer} data: #{@data[@data_pointer]} data_ptr: #{@data_pointer}"
    case instruction
    when :next_data
      @data_pointer += 1

      if @prev_max_data_pointer < @data_pointer
        @data[@data_pointer] = 0
        @prev_max_data_pointer = @data_pointer
      end
    when :prev_data
      raise "Cannot go beyond leftmost data cell. (Data pointer would be -1)" if @data_pointer == 0
      @data_pointer -= 1
    when :inc
      @data[@data_pointer] += 1
      raise "Data is over 1 byte" if @data[@data_pointer] > 255
    when :dec
      @data[@data_pointer] -= 1
      raise "Data is under 1 byte" if @data[@data_pointer] < 0
    when :p_char
      $stdout.write [@data[@data_pointer]].pack('c')
    when :getc
      input = $stdin.gets.chomp.each_byte.first
      unless input
        raise "No data present in STDIN after EOF."
      end

      @data[@data_pointer] = input
    when ForwardJump
      if @data[@data_pointer] == 0
        @instruction_pointer = instruction.target
      end
    when BackwardJump
      if @data[@data_pointer] != 0
        @instruction_pointer = instruction.target
      end
    else
      raise "Unexpected instruction #{@instructions[@instruction_pointer]}" unless @instructions[@instruction_pointer].is_a? Integer
    end
  end
end


str = ARGV[0] || STDIN.read.chomp
#str = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."
# str = "++>++++<[->+<]."
# ++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.
# p Parser.new(Scanner.new(str).result).symbols
Interpreter.new(Parser.new(Scanner.new(str).result).symbols).start
