class Scanner
  class UnequalJumps < StandardError; end

  # I'm lazy to define it properly...
  CONSTANTS = {
    'inc_pointer' => '>',
    'dec_pointer' => '<',
    'inc_byte' => '+',
    'dec_byte' => '-',
    'out_byte' => '.',
    'get_byte' => ',',
    'jmp_forward' => '[',
    'jmp_back' => ']'
  }

  CONSTANTS.each do |k, v|
    self.const_set(k.upcase, v)
  end

  attr_reader :result
  def initialize(input)
    @input = input.clone
    @result = lex(@input)
    pair_jumps!
  end

  # @param input [String]
  def lex(input)
    keywords = CONSTANTS.invert

    input
      .each_char
      .map { |char| keywords[char] || nil }
      .reject(&:nil?)
  end

  # The number of jmp_forward and jmp_back should be equal.
  def pair_jumps!()
    jmp_f_count = @result.filter { |str| str == 'jmp_forward' }.size
    jmp_b_count = @result.filter { |str| str == 'jmp_back' }.size

    raise UnequalJumps.new("Foward and Backward jump counts are not equal") unless jmp_f_count == jmp_b_count
  end
end
